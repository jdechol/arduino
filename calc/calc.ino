//Serial Monitor
// open the serial monitor ,if you input red, you will see the red LED light up
//Email:support@sunfounder.com
//Website:www.sunfounder.com
//2015.5.7
const int size = 9;
int bitArray[size];
const int pins[] = {11,10,9,8,7,6,5,4,3};
String stringInput;
int X;

void setup()
{
  int i;
  for(i = 0; i < size; i++){
    pinMode(pins[i], OUTPUT);
  }
  Serial.begin(9600);  // start serial port at 9600 bps:
  Serial.println("Please input any color of LED:");  //print message on serial monitor
}

void loop()
{
  int i = 0;
  int hi = 1 << size;
  if(Serial.available()>0)    // check if data has been sent from the computer
  {  
    stringInput = "";
    while (Serial.available() > 0)  
    {        
      int inChar = char(Serial.read());
      inChar -= 48;    
      stringInput += inChar;
      X = stringInput.toInt(); 
      X = X % 512;
      delay(2);
    }
    Serial.println(X);
  }
  setBits();
  setPins();
}

void setBits(){
  int i, bitValue;
  for(i = 0; i < size; i++){
    bitValue = (X & (1 << i)) >> i;
    if(bitValue) bitArray[i] = HIGH;
    else bitArray[i] = LOW;
  }
}

void setPins(){
  int i;
  for(i = 0; i < size; i++){
    digitalWrite(pins[i], bitArray[i]);
  }
}

