//Serial Monitor
// open the serial monitor ,if you input red, you will see the red LED light up
//Email:support@sunfounder.com
//Website:www.sunfounder.com
//2015.5.7
const int size = 9;
int bitArray[size];
const int pins[] = {11,10,9,8,7,6,5,4,3};

void setup()
{
  int i;
  for(i = 0; i < size; i++){
    pinMode(pins[i], OUTPUT);
  }
}

void loop()
{
  int i = 0;
  int hi = 1 << size;
  while(true){
    if(i > hi) i = 0;
    setBits(i);
    setPins();
    delay(256-i*.5);
    i++;
  } 
}

void setBits(int X){
  int i, bitValue;
  for(i = 0; i < size; i++){
    bitValue = (X & (1 << i)) >> i;
    if(bitValue) bitArray[i] = HIGH;
    else bitArray[i] = LOW;
  }
}

void setPins(){
  int i;
  for(i = 0; i < size; i++){
    digitalWrite(pins[i], bitArray[i]);
  }
}

